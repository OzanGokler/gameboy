﻿var BrowserUI = function () { };

BrowserUI.prototype = {

    //ASSETS
    bg: null,
    logo: null,
    logoAnim: null,
    ropeLeft: null,
    ropeRight: null,
    fireLeft: null,
    fireLeftAnim: null,
    fireRight: null,
    fireRightAnim: null,
    light: null,
    lightAnim: null,

    //DYNAMIC OBJECTS
    iconBoxes: null,

    //GROUPS
    logoGroup: null,
    gameList: null,
    gameindex: 0,
    boolTween: false,
    currentIconTween: null,
    targetIconBox_X: null,
    iconTweenSpeed: 100,


    preload: function () {
        this.gameList = finder._gameList;
        for (var i = 0; i < this.gameList.length; i++) {

            _loading.loadGameIcons(this.gameList[i]);
        }
    },

    create: function () {
        //bg
        this.bg = browser.add.sprite(0, 0, "background");
        //logo
        this.logo = browser.add.sprite(browser.width / 2, 535, "logo");
        this.logo.anchor.setTo(0.5);
        this.logoAnim = this.logo.animations.add("logoAnim");
        this.logoAnim.play(10, true);
        this.ropeLeft = browser.add.sprite(this.logo.x - 100, this.logo.y - 535, "rope");
        this.ropeRight = browser.add.sprite(this.logo.x + 100, this.logo.y - 535, "rope");

        //logo Group
        this.logoGroup = browser.add.group();
        this.logoGroup.add(this.ropeRight);
        this.logoGroup.add(this.ropeLeft);
        this.logoGroup.add(this.logo);
        this.logoGroup.bringToTop(this.logo);
        browser.add.tween(this.logoGroup).to({ y: -450 }, 3000, Phaser.Easing.Quadratic.Out, true);

        /*this.iconbox = browser.add.sprite(browser.width / 2, -238, "iconbox");
        this.iconbox.anchor.setTo(0.5, 0);
        browser.add.tween(this.iconbox).to({ y: 0 }, 2000, Phaser.Easing.Quadratic.Out, true);*/

        //Fires
        this.fireLeft = browser.add.sprite(0, 220, "fire");
        this.fireLeftAnim = this.fireLeft.animations.add("fireAnim");
        this.fireLeftAnim.play(15, true);

        this.fireRight = browser.add.sprite(browser.width, 220, "fire");
        this.fireRightAnim = this.fireRight.animations.add("fireAnim");
        this.fireRightAnim.play(15, true);
        this.fireRight.scale.setTo(-1, 1);


        //BRING TO TOP POSITIONS

        //Icons
        this.iconBoxes = browser.add.group();
        for (var i = 0; i < this.gameList.length; i++) {
            var tempGroup = browser.add.group();
            var tempIconBox = browser.add.sprite((browser.width / 2) + i * 200, -238, "iconbox");
            tempIconBox.anchor.setTo(0.5, 0);
            tempGroup.add(tempIconBox);
            var tempIcon = browser.add.sprite((browser.width / 2) + i * 200, -45, "icon_" + this.gameList[i]);
            tempIcon.anchor.setTo(0.5);
            tempGroup.add(tempIcon);
            this.iconBoxes.add(tempGroup);
        }

        browser.add.tween(this.iconBoxes).to({ y: 238 }, 3000, Phaser.Easing.Quadratic.Out, true);
        browser.world.bringToTop(this.logoGroup);

        this.light = browser.add.sprite(0, 0, "light");
        this.lightAnim = this.light.animations.add("lightAnim");
        this.lightAnim.play(8, true);

    },



    gameIndexCalculator: function (positive) {
        if (positive) {
            _browserUI.gameindex--;
            if (_browserUI.gameindex < 0) {
                _browserUI.gameindex = 0;

            }
            else
            {
                this.gameIconMoves(200);
            }
            
        }
        else {
            _browserUI.gameindex++;
            if (_browserUI.gameindex > _browserUI.gameList.length - 1) {
                _browserUI.gameindex = _browserUI.gameList.length - 1;
            }
            else
            {
                this.gameIconMoves(-200);
            }
        }

        console.log(this.gameindex);
    },

    gameIconMoves: function (pos) {
        console.log(pos);
        if (!this.boolTween) {
            this.currentIconTween = browser.add.tween(this.iconBoxes).to({ x: this.iconBoxes.position.x + pos }, this.iconTweenSpeed, Phaser.Easing.Quadratic.Out, true).onComplete.add(function () { _browserUI.boolTween = false });
            this.boolTween = true;
            this.targetIconBox_X = this.iconBoxes.position.x + pos;
        } else {
            
            this.currentIconTween._destroy();
            this.iconBoxes.position.x = this.targetIconBox_X;
            this.currentIconTween = browser.add.tween(this.iconBoxes).to({ x: this.iconBoxes.position.x + pos }, this.iconTweenSpeed, Phaser.Easing.Quadratic.Out, true).onComplete.add(function () { _browserUI.boolTween = false });
            this.targetIconBox_X = this.iconBoxes.position.x + pos;
        }    
    },

    openSelectedGame: function(selectedindex) {
        
    }
};
var _browserUI = new BrowserUI();