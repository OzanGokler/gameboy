﻿var Contoller = function () { };

Contoller.prototype = {

    MyGamepad: null,
    boolIndex: true,

    preload:function() {
        //GAMEPAD INITIALIZE
        this.MyGamepad = browser.input.gamepad.pad1;
        browser.input.gamepad.start();
    },

    create: function () {
        
    },

    update: function() {
        this.gameindexCalculator();
    },

    gameindexCalculator: function () {
        //LEFT BUTTON
        if (this.MyGamepad.justPressed(Phaser.Gamepad.XBOX360_DPAD_LEFT) && this.boolIndex === true) {
            _browserUI.gameIndexCalculator(true);          
            this.boolIndex = false;
        }
        //RIGHT BUTTON
        if (this.MyGamepad.justPressed(Phaser.Gamepad.XBOX360_DPAD_RIGHT) && this.boolIndex === true) {
            _browserUI.gameIndexCalculator(false);
            this.boolIndex = false;
        }
        //RIGHT AND LEFT BUTTON RELEASED
        if (this.MyGamepad.justReleased(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || this.MyGamepad.justReleased(Phaser.Gamepad.XBOX360_DPAD_LEFT)) {
            this.boolIndex = true;
        }     
    }   
}

var _contoller = new Contoller();