﻿
﻿var showLogs = true;
if (showLogs) {
    console.log("ScreenManager");
}
var gameWidth = 800;
var gameHeight = 450;
var backgroundColor = "#000000";

var PlayerCharacter;
var klavye;
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, 'KulturGameDev',
    {
        preload: preload,
        create: create,
        update: update
    });

function preload() 
{
    if (showLogs) 
    {
        console.log("ScreenManager ­ preload");
    }

    game.load.image('background', 'Pictures/bg.png');
    game.load.atlas('mario_walking', 'sprites/MarioSprites.png', 'sprites/MarioSprites.json', Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
}

function create() 
{
    if (showLogs)

        console.log("ScreenManager ­ create");

    var arkaplan = game.add.sprite(game.world.centerX, game.world.centerY, 'background');
    
    arkaplan.anchor.setTo(0.5, 0.5);

    klavye = game.input.keyboard.createCursorKeys();
    
    PlayerCharacter = new GameObjects.Character();
    PlayerCharacter.init("Mario");
    console.log("name 5" + PlayerCharacter.GetName());
}

var bFacingRight = true;

function update()
{
    if (klavye.left.isDown) {
        PlayerCharacter.MoveLeft();
    }
    else if (klavye.right.isDown) {
        PlayerCharacter.MoveRight();
    }
    else if (klavye.up.isDown) {
        PlayerCharacter.jump();
    }
    else {
        PlayerCharacter.stand();
    }
}