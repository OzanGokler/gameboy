﻿var Loading = function () { };
Loading.prototype = {
    iconname: ["mario", "galaxy", "fifa", "crash", "nba", "pes", "nfs"],
    iconpath: ["Games/mario/icon.png", "Games/galaxy/icon.png", "Games/fifa/icon.png"
    , "Games/crash/icon.png", "Games/nba/icon.png", "Games/pes/icon.png", "Games/nfs/icon.png"],

    loadAssets: function () {
        //for (var i = 0; i < this.iconname.length; i++) {
        //    browser.load.image(this.iconname[i], this.iconpath[i]);
        //}

        //IMAGERS
        browser.load.image("background", "Assets/Background.png");
        browser.load.image("rope", "Assets/Logo/Rope.png");
        browser.load.image("iconbox", "Assets/IconBox.png");

        //ANIMATIONS
        browser.load.atlasJSONArray("logo",
            ("Assets/Logo/Logo.png"),
            ("Assets/Logo/Logo.json"));
        browser.load.atlasJSONArray("fire",
           ("Assets/Fire/Fire.png"),
           ("Assets/Fire/Fire.json"));
        browser.load.atlasJSONArray("light",
           ("Assets/Fire/Light.png"),
           ("Assets/Fire/Light.json"));
    },

    loadScripts: function () {
        browser.load.script("BrowserManager", "MainClass/BrowserManager.js");
        browser.load.script("BrowserUI", "Class/BrowserUI.js");
        browser.load.script("Controller", "Class/Controller.js");
    },

    StartBrowserStates: function () {
        browser.state.add(gameStates.main, _browserManager);
        browser.state.start(gameStates.main);
    },

    preload: function () {
        this.loadScripts();
        this.loadAssets();
    },

    create: function () {
        this.StartBrowserStates();
    },

    loadGameIcons: function (gameName)
    {
        browser.load.image("icon_" + gameName, "Games/" + gameName + "/icon.png");
        console.log("Games/" + gameName + "/icon.png");
    }
};
var _loading = new Loading();
