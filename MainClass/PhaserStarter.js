﻿var gameWidth = 480;
var gameHeight = 240;
var backgroundColor = "#000000";
var browser = new Phaser.Game(gameWidth, gameHeight, Phaser.CANVAS, "BROWSER"
    /*{ preload: preload, create: create, update: update }*/);
var gameStates = { main: "main", loading: "loading" };



Main = function () { };
Main.prototype =
{

    preload: function () {
        browser.load.script(gameStates.loading, "MainClass/Loading.js");
    },

    create: function ()
    {;
        browser.state.add(gameStates.loading, _loading);
        browser.state.start(gameStates.loading);
    }
};
browser.state.add("Main", Main);
browser.state.start("Main");